#### What is DBMS?
A database management system is system software for creating and managing databases. It is used to manipulate the data in the database.

#### What is normalization?
Normalization is a process of organizing the data in database to avoid data redundancy, data anomalies. It divides larger tables to smaller tables and links them using relationships. 

#### What is file decomposition?
Decomposition is the process of breaking down files. It breaks the files into multiple files in a file system.

#### What is table space?
A tablespace is a storage location where the actual tables underlying database objects are kept. 

#### Difference between each normal form

__1NF__

- An attribute/column of a table cannot hold multiple values - `Atomicity`

__2NF__

- Table should be in 1NF
- No non-key attribute is dependent on the proper subset of any candidate key of table.
    
__3NF__

- Table should be in 2NF
- Transitive functional dependency of non-prime attribute on any super key should be removed.
    
__Boyce-Codd NF__

- A table complies with BCNF if it is in 3NF and for every functional dependency X->Y, X should be the super key of the table.

__4NF__

- There are no non-trivial multivalued dependencies other than a candidate key. It builds on the first three normal forms and the Boyce-Codd Normal Form.

__5NF__

- The relation for the super-key Should have a primary key based on 2 column relation.
- It has multi-valued dependencies.