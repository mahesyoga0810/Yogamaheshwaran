## Assignment 1 

1.Write a query which will display the customer id, account type they hold, their account number and bank name.

```sql
SELECT  A.CUSTOMER_ID ID, A.ACCOUNT_TYPE TYPE, A.ACCOUNT_NO "ACCOUNT NO", B.BANK_NAME
FROM ACCOUNT_INFO A
INNER JOIN BANK_INFO B
ON A.IFSC_CODE = B.IFSC_CODE;
```

2.Write a query which will display the customer id, account type and the account number of HDFC customers who registered after 12-JAN-2012 and before 04-APR-2012.

```sql
SELECT A.CUSTOMER_ID ID, A.ACCOUNT_TYPE TYPE, A.ACCOUNT_NO
FROM ACCOUNT_INFO A
WHERE A.IFSC_CODE IN (
SELECT IFSC_CODE 
FROM BANK_INFO 
WHERE UPPER(BANK_NAME)='HDFC' 
AND A.REGISTRATION_DATE BETWEEN TO_DATE('12-JAN-2012','DD-MON-YYYY') 
AND  TO_DATE('04-APR-2012','DD-MON-YYYY')
);
```

3.Write a query which will display the customer id, customer name, account no, account type and bank name where the customers hold the account.

```sql
SELECT CUST.CUSTOMER_ID ID, CUST.CUSTOMER_NAME NAME, ACC.ACCOUNT_NO "ACC. NO.", ACC.ACCOUNT_TYPE TYPE, BANK.BANK_NAME
FROM CUSTOMER_PERSONAL_INFO CUST 
INNER JOIN ACCOUNT_INFO ACC
ON (CUST.CUSTOMER_ID = ACC.CUSTOMER_ID)
INNER JOIN BANK_INFO BANK
ON (BANK.IFSC_CODE = ACC.IFSC_CODE);
```

4.Write a query which will display the customer id, customer name, gender, marital status along with the unique reference string and sort the records based on customer id in descending order

```sql
SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, CUST.GENDER, CUST.MARITAL_STATUS, CUST.CUSTOMER_NAME||'_'||CUST.GENDER||'_'||CUST.MARITAL_STATUS "UNIQUE_REF_STRING"
FROM CUSTOMER_PERSONAL_INFO CUST
ORDER BY CUST.CUSTOMER_ID DESC;
```

5.Write a query which will display the account number, customer id, registration date, initial deposit amount of the customer whose initial deposit amount is within the range of Rs.15000 to Rs.25000.

```sql
SELECT ACC.ACCOUNT_NO, ACC.CUSTOMER_ID, ACC.REGISTRATION_DATE, ACC.INITIAL_DEPOSIT
FROM ACCOUNT_INFO ACC
WHERE ACC.INITIAL_DEPOSIT BETWEEN 15000 AND 25000;
```

6.Write a query which will display customer id, customer name, date of birth, guardian name of the customers whose name starts with 'J'.

```sql
SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, CUST.DATE_OF_BIRTH DOB, CUST.GUARDIAN_NAME
FROM CUSTOMER_PERSONAL_INFO CUST
WHERE UPPER(CUST.CUSTOMER_NAME) LIKE 'J%';
```

7.Write a query which will display customer id, account number and passcode

```sql
SELECT ACC.CUSTOMER_ID, ACC.ACCOUNT_NO,
SUBSTR(ACC.CUSTOMER_ID,-3)||SUBSTR(ACC.ACCOUNT_NO,-4) "PASSCODE"
FROM ACCOUNT_INFO ACC;
```

8.Write a query which will display the customer id, customer name, date of birth, Marital Status, Gender, Guardian name, contact no and email id of the customers whose gender is male 'M' and marital status is MARRIED.

```sql
SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, CUST.DATE_OF_BIRTH, CUST.MARITAL_STATUS, CUST.GENDER, CUST.GUARDIAN_NAME, CUST.CONTACT_NO, CUST.MAIL_ID
FROM CUSTOMER_PERSONAL_INFO CUST
WHERE UPPER(CUST.GENDER)='M'
AND UPPER(CUST.MARITAL_STATUS)='MARRIED';
```

9.Write a query which will display the customer id, customer name, guardian name, reference account holders name of the customers who are referenced / referred by their 'FRIEND'.

```sql
SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, CUST.GUARDIAN_NAME, REF.REFERENCE_ACC_NAME
FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN CUSTOMER_REFERENCE_INFO REF
ON (CUST.CUSTOMER_ID = REF.CUSTOMER_ID AND REF.RELATION='FRIEND');
```

10.Write a query to display the customer id, account number and interest amount in the below format with INTEREST_AMT as alias name, sort the result based on the INTEREST_AMT in ascending order

```sql
SELECT ACC.CUSTOMER_ID, ACC.ACCOUNT_NO, '$'||CEIL(ACC.INTEREST) INTEREST_AMT
FROM ACCOUNT_INFO ACC
ORDER BY ACC.INTEREST;
```

11.Write a query which will display the customer id, customer name, account no, account type, activation date, bank name whose account will be activated on '10-APR-2012'

```sql
SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, ACC.ACCOUNT_NO, ACC.ACCOUNT_TYPE, ACC.ACTIVATION_DATE, BANK.BANK_NAME FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN ACCOUNT_INFO ACC
ON (CUST.CUSTOMER_ID = ACC.CUSTOMER_ID 
AND ACC.ACTIVATION_DATE=TO_DATE('10-APR-2012','DD-MON-YYYY'))
INNER JOIN BANK_INFO BANK
ON (ACC.IFSC_CODE = BANK.IFSC_CODE);
```

12.Write a query which will display account number, customer id, customer name, bank name, branch name, ifsc code, citizenship, interest and initial deposit amount of all the customers.

```sql
SELECT ACC.ACCOUNT_NO, CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, BANK.BANK_NAME, BANK.BRANCH_NAME,
BANK.IFSC_CODE, CUST.CITIZENSHIP, ACC.INTEREST, ACC.INITIAL_DEPOSIT
FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN ACCOUNT_INFO ACC
ON (CUST.CUSTOMER_ID = ACC.CUSTOMER_ID)
INNER JOIN BANK_INFO BANK
ON (ACC.IFSC_CODE = BANK.IFSC_CODE);
```

13.Write a query which will display customer id, customer name, date of birth, guardian name, contact number, mail id and reference account holder's name of the customers who has submitted the passport as an identification document.

```sql
SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, CUST.DATE_OF_BIRTH, CUST.GUARDIAN_NAME, CUST.CONTACT_NO, CUST.MAIL_ID, REF.REFERENCE_ACC_NAME
FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN CUSTOMER_REFERENCE_INFO REF
ON (CUST.CUSTOMER_ID = REF.CUSTOMER_ID)
WHERE UPPER(CUST.IDENTIFICATION_DOC_TYPE)='PASSPORT';
```

14.Write a query to display the customer id, customer name, account number, account type, initial deposit, interest who have deposited maximum amount in the bank.

```sql
SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, ACC.ACCOUNT_NO, ACC.ACCOUNT_TYPE, ACC.INITIAL_DEPOSIT, ACC.INTEREST 
FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN ACCOUNT_INFO ACC
ON (CUST.CUSTOMER_ID=ACC.CUSTOMER_ID)
WHERE CUST.CUSTOMER_ID = (
SELECT CUSTOMER_ID 
FROM ACCOUNT_INFO 
WHERE INITIAL_DEPOSIT=(
SELECT MAX(INITIAL_DEPOSIT) 
FROM ACCOUNT_INFO));
```

15.Write a query to display the customer id, customer name, account number, account type, interest, bank name and initial deposit amount of the customers who are getting maximum interest rate.

```sql
SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, ACC.ACCOUNT_NO, ACC.ACCOUNT_TYPE, ACC.INTEREST, BANK.BANK_NAME, ACC.INITIAL_DEPOSIT
FROM CUSTOMER_PERSONAL_INFO CUST 
INNER JOIN ACCOUNT_INFO ACC ON(ACC.CUSTOMER_ID=CUST.CUSTOMER_ID)
INNER JOIN BANK_INFO BANK ON(BANK.IFSC_CODE=ACC.IFSC_CODE)
WHERE CUST.CUSTOMER_ID = (
SELECT CUSTOMER_ID
FROM ACCOUNT_INFO
WHERE INTEREST=(
SELECT MAX(INTEREST)
FROM ACCOUNT_INFO));
```


16.Write a query to display the customer id, customer name, account no, bank name, contact no and mail id of the customers who are from BANGALORE.

```sql
SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, ACC.ACCOUNT_NO, BANK.BANK_NAME, CUST.CONTACT_NO, CUST.MAIL_ID FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN ACCOUNT_INFO ACC
ON (ACC.CUSTOMER_ID = CUST.CUSTOMER_ID)
INNER JOIN BANK_INFO BANK
ON (BANK.IFSC_CODE = ACC.IFSC_CODE)
WHERE INSTR(CUST.ADDRESS,'BANGALORE')>0;
```

17.Write a query which will display customer id, bank name, branch name, ifsc code, registration date, activation date of the customers whose activation date is in the month of march (March 1'st to March 31'st).

```sql
SELECT A.CUSTOMER_ID, B.BANK_NAME, B.BRANCH_NAME, B.IFSC_CODE, A.REGISTRATION_DATE, A.ACTIVATION_DATE
FROM ACCOUNT_INFO A, BANK_INFO B 
WHERE A.IFSC_CODE=B.IFSC_CODE 
AND EXTRACT(MONTH FROM A.ACTIVATION_DATE)=3;
```

18.Write a query which will calculate the interest amount and display it along with customer id, customer name, account number, account type, interest, and initial deposit amount.<BR>Hint :Formula for interest amount, calculate: ((interest/100) * initial deposit amt) with column name 'interest_amt' (alias)

```sql
SELECT ((B.INTEREST/100) * B.INITIAL_DEPOSIT) INTEREST_AMT, A.CUSTOMER_ID, A.CUSTOMER_NAME,
B.ACCOUNT_NO, B.ACCOUNT_TYPE, B.INTEREST, B.INITIAL_DEPOSIT 
FROM CUSTOMER_PERSONAL_INFO A, ACCOUNT_INFO B
WHERE A.CUSTOMER_ID = B.CUSTOMER_ID;  
```

19.Write a query to display the customer id, customer name, date of birth, guardian name, contact number, mail id, reference name who has been referenced by 'RAGHUL'.

```sql
SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME, C.DATE_OF_BIRTH, C.GUARDIAN_NAME, C.CONTACT_NO, C.MAIL_ID, R.REFERENCE_ACC_NAME
FROM CUSTOMER_PERSONAL_INFO C, CUSTOMER_REFERENCE_INFO R
WHERE C.CUSTOMER_ID=R.CUSTOMER_ID 
AND R.REFERENCE_ACC_NAME='RAGHUL';
```
20.Write a query which will display the customer id, customer name and contact number with ISD code of all customers in below mentioned format.  Sort the result based on the customer id in descending order. 

```sql
SELECT CUSTOMER_ID, CUSTOMER_NAME, CONTACT_NO, ('+91-' ||SUBSTR(CONTACT_NO, 1, 3)|| '-' || SUBSTR(CONTACT_NO, 4, 3) || '-' || SUBSTR(CONTACT_NO, 7, 4)) AS CONTACT_ISD
FROM CUSTOMER_PERSONAL_INFO;
```

21.Write a query which will display account number, account type, customer id, customer name, date of birth, guardian name, contact no, mail id , gender, reference account holders name, reference account holders account number, registration date, activation date, number of days between the registration date and activation date with alias name "NoofdaysforActivation", 
bank name, branch name and initial deposit for all the customers.


```sql
SELECT A.ACCOUNT_NO, A.ACCOUNT_TYPE, A.CUSTOMER_ID, C.CUSTOMER_NAME, C.DATE_OF_BIRTH, C.GUARDIAN_NAME, C.CONTACT_NO, C.MAIL_ID , C.GENDER, R.REFERENCE_ACC_NAME, R.REFERENCE_ACC_NO, A.REGISTRATION_DATE, A.ACTIVATION_DATE,
TRUNC( ACTIVATION_DATE ) - TRUNC( REGISTRATION_DATE ) AS "NOOFDAYSFORACTIVATION",
B.BANK_NAME, B.BRANCH_NAME
FROM  CUSTOMER_PERSONAL_INFO C, ACCOUNT_INFO A, CUSTOMER_REFERENCE_INFO R, BANK_INFO B
WHERE C.CUSTOMER_ID=A.CUSTOMER_ID
AND A.CUSTOMER_ID=R.CUSTOMER_ID
AND A.IFSC_CODE=B.IFSC_CODE;
```

22.Write a query which will display customer id, customer name,  guardian name, identification doc type, reference account holders name, account type, ifsc code, bank name and current balance for the customers who has only the savings account. 

```sql
SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME,  C.GUARDIAN_NAME, C.IDENTIFICATION_DOC_TYPE, R.REFERENCE_ACC_NAME, A.ACCOUNT_TYPE, B.IFSC_CODE, B.BANK_NAME, 
TRUNC (A.INITIAL_DEPOSIT+((A.INITIAL_DEPOSIT*INTEREST)/100)) AS "CURRENT_BALANCE" 
FROM CUSTOMER_PERSONAL_INFO C, ACCOUNT_INFO A, CUSTOMER_REFERENCE_INFO R, BANK_INFO B
WHERE C.CUSTOMER_ID=A.CUSTOMER_ID
AND A.CUSTOMER_ID=R.CUSTOMER_ID
AND A.IFSC_CODE=B.IFSC_CODE
AND UPPER(A.ACCOUNT_TYPE)='SAVINGS';
```

23."Write a query which will display the customer id, customer name, account number, account type, interest, initial deposit;check the initial deposit, <br/> if initial deposit is 20000 then display ""high"",<br/> if initial deposit is 16000 display 'moderate',<br/> if initial deposit is 10000 display 'average', <br/>if initial deposit is 5000 display 'low', <br/>if initial deposit is 0 display 'very low' otherwise display 'invalid' and sort by interest in descending order.<br/>
 
```sql
SELECT A.CUSTOMER_ID,A.CUSTOMER_NAME,B.ACCOUNT_NO,B.ACCOUNT_TYPE,B.INTEREST,B.INITIAL_DEPOSIT,
CASE WHEN B.INITIAL_DEPOSIT = 20000 THEN'HIGH'
WHEN B.INITIAL_DEPOSIT = 16000 THEN'MODERATE'
WHEN B.INITIAL_DEPOSIT = 10000 THEN'AVERAGE'
WHEN B.INITIAL_DEPOSIT = 5000 THEN'LOW'
WHEN B.INITIAL_DEPOSIT = 0 THEN'VERY LOW'
ELSE 'INVALID' END AS "DEPOSIT_STATUS" 
FROM CUSTOMER_PERSONAL_INFO A 
INNER JOIN ACCOUNT_INFO B 
ON(A.CUSTOMER_ID=B.CUSTOMER_ID)
ORDER BY B.INTEREST DESC;
```

24.Write a query which will display customer id, customer name,  account number, account type, bank name, ifsc code, initial deposit amount and new interest amount for the customers whose name starts with ""J"".

```sql
SELECT  A.CUSTOMER_ID, A.CUSTOMER_NAME, B.ACCOUNT_NO, B.ACCOUNT_TYPE, C.BANK_NAME, B.IFSC_CODE, '$'||B.INITIAL_DEPOSIT,
ROUND(
DECODE(B.ACCOUNT_TYPE,'SAVINGS',
(B.INTEREST + (B.INTEREST*B.INTEREST/100)),B.INTEREST),
2) AS NEW_INTEREST
FROM CUSTOMER_PERSONAL_INFO A, ACCOUNT_INFO B, BANK_INFO C
WHERE (A.CUSTOMER_ID = B.CUSTOMER_ID 
AND B.IFSC_CODE=C.IFSC_CODE 
AND A.CUSTOMER_NAME LIKE 'J%');
```


25.Write query to display the customer id, customer name, account no, initial deposit, tax percentage as calculated.

```sql
SELECT A.CUSTOMER_ID, A.CUSTOMER_NAME, B.ACCOUNT_NO, B.INITIAL_DEPOSIT,
CASE WHEN B.INITIAL_DEPOSIT = 0 THEN '0%'
WHEN B.INITIAL_DEPOSIT <= 10000 THEN '3%'
WHEN B.INITIAL_DEPOSIT > 10000 AND B.INITIAL_DEPOSIT < 20000 THEN '5%'
WHEN B.INITIAL_DEPOSIT >= 20000 AND B.INITIAL_DEPOSIT <= 30000 THEN '7%'
ELSE '10%' END "TAX PERCENTAGE"
FROM CUSTOMER_PERSONAL_INFO A, ACCOUNT_INFO B
WHERE A.CUSTOMER_ID = B.CUSTOMER_ID;
```