package exe;

public class Email {
	private To to;
	private From from;
	private Subject subject;
	private Body body;
	
	public Email() {}
	@Override
	public String toString() {
		return "Email [to=" + to + ", from=" + from + ", subject=" + subject + ", body=" + body + "]";
	}
	public Email(To to, From from, Subject subject, Body body) {
		super();
		this.to = to;
		this.from = from;
		this.subject = subject;
		this.body = body;
	}
	
	public To getTo() {
		return to;
	}

	public From getFrom() {
		return from;
	}

	public Subject getSubject() {
		return subject;
	}

	public Body getBody() {
		return body;
	}

	public void setTo(To to) {
		this.to = to;
	}

	public void setFrom(From from) {
		this.from = from;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	
}
