package com;


import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


public class Client {

	public static void main(String[] args) {

//		Customer customer = new Customer();
//		customer.setCustomerName("Rahul");
//		System.out.println(customer.getCustomerName());
		Customer customer;
		
		Resource resource = new ClassPathResource("beans.xml");
		BeanFactory factory = new XmlBeanFactory(resource);
		
//		customer = (Customer) factory.getBean("cust");
//		customer.setCustomerName("Raaaa");
//		System.out.println(customer.getCustomerName());
//		
//		customer = (Customer) factory.getBean("custPar");
//		System.out.println(customer.getCustomerName());
//		
//		customer = (Customer) factory.getBean("custSet");
//		System.out.println(customer.getCustomerName());
//		
		customer = (Customer) factory.getBean("custBank");
		System.out.println(customer);
	}

}
