package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.model.Guest;

@Controller
public class InsertController {

	
	@RequestMapping("/insert")
	public ModelAndView getA() {
		ModelAndView view = new ModelAndView();
		view.setViewName("in");
		view.addObject("message","Good morning from controller");
		return view;
	}
	
	@RequestMapping("/guest")
	public ModelAndView getGuestView(Guest guest) {
		ModelAndView view = new ModelAndView();
		view.setViewName("guestDetails");
		view.addObject("guest",guest);
		view.addObject("message","Good morning");
		return view;
	}
}
