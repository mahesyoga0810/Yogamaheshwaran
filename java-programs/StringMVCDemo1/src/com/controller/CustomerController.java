package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.javatraining.customer.model.Customer;
import com.javatraining.customer.service.CustomerService;

@Controller
public class CustomerController {

	@Autowired
	CustomerService customerService;
	
	
	@RequestMapping("/CustomerInfo")
	public ModelAndView getGuestView(Customer customer) {
		customerService.insertCustomer(customer);	
		ModelAndView view = new ModelAndView();
		view.setViewName("customerDetails");
		view.addObject("message",customer.getCustomerName() + ", Inserted");
		return view;
	}
}
