package com.javatraining.customer.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.model.Customer;
import com.javatraining.customer.dbcom.DBconfig;

public class CustomerDAOImpl implements CustomerDAO {

//	Insert New Customer
	public int insertCustomer(Customer customer) {
		Connection conn = DBconfig.getConnection();
		int rows = 0;

		try {

			PreparedStatement statement = conn.prepareStatement("insert into customer values(?,?,?,?)");
			statement.setInt(1, customer.getCustomerId());
			statement.setString(2, customer.getCustomerName());
			statement.setString(3, customer.getCustomerAddress());
			statement.setInt(4, customer.getBillAmount());
			
			rows = statement.executeUpdate();
			
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rows;
	}

//	Update an existing customer
	public int updateCustomer(int customerId, String newCustomerAddress, int newBillAmount) {
		Connection conn = DBconfig.getConnection();
		int rows = 0;

		try {

			PreparedStatement statement = conn.prepareStatement("update customer set customerAddress=?, billAmount=? where customerId=?");
			statement.setString(1, newCustomerAddress);
			statement.setInt(2, newBillAmount);
			statement.setInt(3, customerId);
			
			rows = statement.executeUpdate();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rows;
	}

//	Delete an existing customer
	public int deleteCustomer(int customerId) {
		Connection conn = DBconfig.getConnection();
		PreparedStatement statement =null;
		int rows = 0;
		
		try {
			
			statement = conn.prepareStatement("delete from customer where customerId=?");
			statement.setInt(1, customerId);
			
			rows = statement.executeUpdate();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return rows;
	}

//	Find Customer by ID
	public Customer findByCustomerId(int customerId) {
		Connection conn = DBconfig.getConnection();
		PreparedStatement statement =null;
		Customer customer = null;
		
		try {
			
			statement = conn.prepareStatement("select * from customer where customerId=?");
			statement.setInt(1, customerId);
			
			ResultSet result = statement.executeQuery();
			if(result.next()) {				
				customer = new Customer();				
				customer.setCustomerId(result.getInt(1));
				customer.setCustomerName(result.getString(2));
				customer.setCustomerAddress(result.getString(3));
				customer.setBillAmount(result.getInt(4));
			}
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return customer;
	}

//	Check if customer exists
	@Override
	public boolean isCustomerExists(int customerId) {
		Connection conn = DBconfig.getConnection();
		PreparedStatement statement =null;
		ResultSet result = null;
		boolean isCustomer=false;
		try {
			
			statement = conn.prepareStatement("select * from customer where customerId=?");
			statement.setInt(1, customerId);
			result = statement.executeQuery();
			isCustomer = result.next();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
		return isCustomer ;
	}

//	List all the customers
	@Override
	public List<Customer> listAllCustomers() {
		Connection conn = DBconfig.getConnection();
		String query = "select * from customer";
		List<Customer> allCustomers = new ArrayList<Customer>();
		try {
			Statement statement = conn.createStatement();
			ResultSet result = statement.executeQuery(query);
			
			while(result.next()) {
				Customer customer = new Customer();
				customer.setCustomerId(result.getInt(1));
				customer.setCustomerName(result.getString(2));
				customer.setCustomerAddress(result.getString(3));
				customer.setBillAmount(result.getInt(4));
				
				allCustomers.add(customer);
				customer=null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return allCustomers;
	}


}
