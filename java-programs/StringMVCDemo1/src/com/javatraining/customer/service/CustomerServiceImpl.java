package com.javatraining.customer.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.model.Customer;

public class CustomerServiceImpl implements CustomerService {
	@Autowired
	CustomerDAO customerDAO;

	@Override
	public int insertCustomer(Customer customer) {
		return customerDAO.insertCustomer(customer);
	}

	@Override
	public int updateCustomer(int customerId, String newCustomerAddress, int newBillAmount) {
		return customerDAO.updateCustomer(customerId, newCustomerAddress, newBillAmount);
	}

	@Override
	public int deleteCustomer(int customerId) {
		return customerDAO.deleteCustomer(customerId);
	}

	@Override
	public Customer findByCustomerId(int customerId) {
		return customerDAO.findByCustomerId(customerId);
	}

	@Override
	public boolean isCustomerExists(int customerId) {
		return customerDAO.isCustomerExists(customerId);
	}

	@Override
	public List<Customer> listAllCustomers() {
		return customerDAO.listAllCustomers();
	}

}
