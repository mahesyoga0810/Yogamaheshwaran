<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<h1>Hello, <%= session.getAttribute("username") %></h1>
<br><br>
<h2> TOTAL BILL AMOUNT: </h2>
<%
	int total=0;
	String itemsPrice[] = request.getParameterValues("gun");
	if(itemsPrice != null){
		for(String price: itemsPrice ){
			total += Integer.parseInt(price);
		}
	}

%>
<h1><%= total %></h1>
</body>
</html>