package com;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.javatraining.customer.dao.CustomerDAO;
import com.javatraining.customer.impl.CustomerDAOImpl;
import com.javatraining.customer.model.Customer;

/**
 * Servlet implementation class UpdateCustomer
 */
public class UpdateCustomer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCustomer() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		int customerId = Integer.parseInt(req.getParameter("customerId"));
		String customerAddress =  req.getParameter("customerAddress");
		int billAmount = Integer.parseInt(req.getParameter("billAmount"));

		
		CustomerDAO customerDAO = new CustomerDAOImpl();
		if(customerDAO.isCustomerExists(customerId)) {
			int result = customerDAO.updateCustomer(customerId, customerAddress, billAmount);
			
			res.setContentType("text/html");
			res.getWriter().println("<font color=green><h1>"+result + " row(s) updated</h1></font>");	
		
		} else {
			res.getWriter().println("Customer with "+customerId+ "does not exists" );
		}
	}


}
