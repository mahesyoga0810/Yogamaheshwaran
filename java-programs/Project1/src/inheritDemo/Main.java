package inheritDemo;

public class Main {
	public static void main(String[] args) {
		Vehicle vehicle = new Car();
		vehicle.color = "Red";
		vehicle.noOfWheels = 4;
		vehicle.start();
		vehicle.showDetails();
		
//		vehicle cannot access car properties/methods
//		vehicle.carType = 10 // Doesn't work
		
		
		System.out.println("\n\n");
		Bike bike = new Bike() {
			public void kickStart() {
				System.out.println("Kick start");
			}
			
		};
		
		bike.kickStart();
		
		System.out.println("\n\n");
		Pulsar pulsar = new Pulsar();
		pulsar.color = "Grey";
		pulsar.showDetails();
		pulsar.kickStart();
		pulsar.start();
		
		
//		Dynamic overloading
		vehicle = new Pulsar();
		vehicle.start();
	}
}
