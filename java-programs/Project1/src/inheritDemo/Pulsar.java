package inheritDemo;

public class Pulsar extends Bike {

	@Override
	public void kickStart() {
		System.out.println("Pulsar kick started");
	}

}
