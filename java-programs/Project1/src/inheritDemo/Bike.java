package inheritDemo;

public abstract class Bike extends Vehicle {
	Bike(){		
		noOfWheels = 2;
	}
	
	@Override
	public void start() {
		// TODO Auto-generated method stub
		System.out.println("Bike started");
	}
	
	public abstract void kickStart();

}
