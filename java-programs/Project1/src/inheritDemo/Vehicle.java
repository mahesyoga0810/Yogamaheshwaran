package inheritDemo;

public abstract class Vehicle {
	String color;
	int noOfWheels;
	
	public abstract void start();
	
	public void showDetails() {
		System.out.println("Color : " + color);
		System.out.println("Number of wheels : " + noOfWheels);
	}
}
