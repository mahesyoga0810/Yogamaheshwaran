package io;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DataInputStreamDemo {

	public static void main(String[] args) throws IOException {
		DataInputStream stream = new DataInputStream(
				new BufferedInputStream(
						new FileInputStream(new File("rec.txt"))
				));
		int sal = stream.readInt();
		boolean married = stream.readBoolean();
		double avg = stream.readDouble();
		stream.close();
		
		System.out.println("Salary is " + sal + "\nmarried ? "+married +"\navg = "+avg);
		
	}

}
