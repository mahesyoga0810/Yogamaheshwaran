package io;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ObjectOutputStreamDemo {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		Customer c = new Customer(101, "ABC", "BLR", 80000);
		ObjectOutputStream stream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File("cust.txt"))));
		stream.writeObject(c);
		stream.close();
		
	}

}
