package io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ObjectInputStreamDemo {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		ObjectInputStream stream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File("cust.txt"))));
		Customer c = (Customer) stream.readObject();
		stream.close();
		System.out.println(c);
	}

}
