package interfaceDemo;

public interface Radio {
	
//	All properties are public static final by default
	float frequency =106.4f;
	
//	All behaviours are public abstract by default
	void play();

}
