package jdbcDemo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import config.DBconfig;


public class Demo1 {

	public static void main(String[] args) throws SQLException {
		
		Connection connection = DBconfig.getConnection();
		Statement s = connection.createStatement();
		
		ResultSet rs = s.executeQuery("select * from customer");
		
		while(rs.next()) {
			System.out.println(rs.getString(1) + "\t"  + rs.getString(2) + "\t" +rs.getString(3) + "\t" +rs.getString(4));
		}
	}
}

