package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class  DBconfig {
	public static Connection getConnection() {
		Connection connection= null;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:orcl", "scott", "tiger");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}			
		
		return connection;
	}
}
