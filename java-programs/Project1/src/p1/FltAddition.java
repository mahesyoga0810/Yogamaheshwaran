package p1;

public class FltAddition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if (args.length != 2) {
			System.out.println("Enter 2 arguments");
			return;
		}
		
		float a = Float.parseFloat(args[0]);
		float b = Float.parseFloat(args[1]);
		
		System.out.println(a+" +  "+b+" = "+(a+b));
	}

}
