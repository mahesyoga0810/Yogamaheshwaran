package p1;

public class Demo3 {

	public static void main(String[] args) {
		int n = 10;
		System.out.println(Integer.toBinaryString(n));
		System.out.println(Integer.toOctalString(n));
		System.out.println(Integer.toHexString(n));
	}

}
