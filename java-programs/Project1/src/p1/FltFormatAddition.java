package p1;
import java.text.DecimalFormat;

public class FltFormatAddition {
	public static void main(String[] args) {

		if (args.length != 2) {
			System.out.println("Enter 2 arguments");
			return;
		}

		DecimalFormat df = new DecimalFormat(".##");
		float a = Float.parseFloat(args[0]);
		float b = Float.parseFloat(args[1]);
		
		System.out.println(df.format(a)+" +  "+df.format(b)+" = "+df.format(a+b));
	}
}
