package wrapperClass;

public class Demo {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		String marks = "98";
		System.out.println(Integer.parseInt(marks)+10);
		
		int m = Integer.parseInt(marks);
//		Boxing => Primitive to Object
		Integer x = m; //auto boxing
		Integer y = new Integer(x); //boxing
		
		
		
//		Unboxing => object -> Primitive
		Integer score =10;
		int a = score; // auto unboxing
		int b = Integer.valueOf(score); //unboxing
	}

}
