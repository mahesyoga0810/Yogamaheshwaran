package threadDemo;

public class Demo2 extends Thread {
	static int i =1;
	public Demo2() {
		super("ITPL"+i);
		start();
		i++;
	}
		
	@Override
	public void run() {
		display(Thread.currentThread().getName(), "Hi", "Hello");
		System.out.println("Run called by " + Thread.currentThread().getName());
	}

	private static synchronized void display(String threadName, String message1, String message2) {
		System.out.println("Thread Name: "+ threadName);
		System.out.println("Message 1:"+ message1);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Message 2:"+ message2);
	}
	
	
	public static void main(String[] args) {
		new Demo2();
		new Demo2();
		new Demo2();
		System.out.println("Run called by " + Thread.currentThread().getName());
	}

}
