package exercise;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import config.DBconfig;
import io.IO;

public class Exe4 {

	public static void main(String[] args) {
		
		System.out.print("Enter customer id to Update: ");
		int cId =IO.read.nextInt();
		
		
		
		
		Connection conn = DBconfig.getConnection();
		Statement s =null;
		ResultSet rs = null;
		try {
			s = conn.createStatement();
			rs = s.executeQuery("select * from customer where customerId="+cId);
			if(rs.next()) {
				System.out.println("Welcome, "+rs.getString(2));
				
				System.out.print("Enter new address: ");
				String cAdd =IO.read.next();
				
				System.out.print("Enter new bill amount: ");
				int bAmt =IO.read.nextInt();
				
				PreparedStatement statement = conn.prepareStatement("update customer set customerAddress=?, billAmount=? where customerId=?");
				statement.setString(1, cAdd);
				statement.setInt(2, bAmt);
				statement.setInt(3, cId);
				statement.executeUpdate();
				
				System.out.println("customerId with "+ cId+ " updated with new address: "+ cAdd+ " and new bill amount: "+bAmt);
			} else {
				System.out.println("Customer Id "+ cId + " does not exists");
			}
			conn.close();
			s.close();
			rs.close();
		} catch (SQLException e) {
			System.out.println("Error changing data");
			e.printStackTrace();
		} 
		
	}

}
