package exercise;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import config.DBconfig;
import io.IO;

public class Exe5 {
public static void main(String[] args) {
		
		System.out.print("Enter customer id to DELETE: ");
		int cId =IO.read.nextInt();
		
		
		
		
		Connection conn = DBconfig.getConnection();
		Statement s =null;
		int rs;
		try {
			s = conn.createStatement();
			rs = s.executeUpdate("delete from customer where customerId="+cId);
			if(rs > 0) {
				System.out.println("customerId with "+ cId+ " DELETE");
			} else {
				System.out.println("Customer Id "+ cId + " does not exists");
			}
			conn.close();
			s.close();
		} catch (SQLException e) {
			System.out.println("Error changing data");
			e.printStackTrace();
		} 
		
	}
}
