package exercise;

public class Exe1 {
	public static void main(String[] args) {
		String str = "The quick brown fox jumps over the lazy dog";
//		1.	Print the character at the 12th index.
		System.out.println("1.	Print the character at the 12th index.");
		System.out.println(str.charAt(12));
		
		
//		2.	Check whether the String contains the word �is�. 
		
		System.out.println("2. Check whether the String contains the word �is�.");
		System.out.println(str.contains("is"));
		System.out.println("\n\n");
		
		
//		3.	Add the string �and killed it� to the existing string.
		System.out.println("3. Add the string �and killed it� to the existing string");
		System.out.println(str.concat(" and killed it"));
		System.out.println("\n\n");
		
//		4.	Check whether the String ends with the word �dogs�.
		System.out.println("4. Check whether the String ends with the word �dogs�.");
		System.out.println(str.endsWith("dogs"));
		System.out.println("\n\n");
		
//		5.	Check whether the String is equal to �The quick brown Fox jumps over the lazy Dog�.
		System.out.println("5. Check whether the String is equal to �The quick brown Fox jumps over the lazy Dog�.");
		System.out.println(str.equals("The quick brown Fox jumps over the lazy Dog"));
		System.out.println("\n\n");
		
//		6.	Check whether the String is equal to �THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG�.
		System.out.println("6. Check whether the String is equal to �THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG�.");
		System.out.println(str.equals("THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG"));
		System.out.println("\n\n");
		
//		7.	Find the length of the String. 
		System.out.println("7. Find the length of the String. ");
		System.out.println(str.length());
		System.out.println("\n\n");
		
//		8.	Check whether the String matches to �The quick brown Fox jumps over the lazy Dog�.
		System.out.println("8. Check whether the String matches to �The quick brown Fox jumps over the lazy Dog�.");
		System.out.println(str.matches("The quick brown Fox jumps over the lazy Dog"));
		System.out.println("\n\n");
		
//		9.	Replace the word �The� with the word �A�. 
		System.out.println("9. Replace the word �The� with the word �A�. ");
		System.out.println(str.replace("The", "A"));
		System.out.println("\n\n");
		
//		10.	Split the above string into two such that two animal names do not come together.
		System.out.println("10. Split the above string into two such that two animal names do not come together.");
		String[] s = str.split("over");
		System.out.println(s[0]+  "\n" + s[1]);
		System.out.println("\n\n");
		
//		11.	Print the animal names alone separately from the above string.
		
//		String noFox = str.replace("fox", "").replace("dog", "");
		System.out.println("11. Print the animal names alone separately from the above string.");
		System.out.println(str.substring(str.indexOf("fox"), str.indexOf("fox")+3));
		System.out.println(str.substring(str.indexOf("dog"), str.indexOf("dog")+3));
		System.out.println("\n\n");
		
//		12.	Print the above string in completely lower case.
		System.out.println("12. Print the above string in completely lower case.");
		System.out.println(str.toLowerCase());
		System.out.println("\n\n");
		
//		13.	Print the above string in completely upper case.
		System.out.println("13. Print the above string in completely upper case.");
		System.out.println(str.toUpperCase());
		System.out.println("\n\n");
		
//		14.	Find the index position of the character �a�.
		System.out.println("14.	Find the index position of the character �a�.");
		System.out.println(str.indexOf("a"));
		System.out.println("\n\n");
		
//		15.	Find the last index position of the character �e�. 
		System.out.println("15.	Find the last index position of the character �e�. ");
		System.out.println(str.lastIndexOf("e"));
		System.out.println("\n\n");
		

	}

}
