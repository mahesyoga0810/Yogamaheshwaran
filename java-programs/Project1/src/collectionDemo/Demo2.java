package collectionDemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import comparatorDemo.AddressComparator;
import comparatorDemo.BillAmountComparator;
import getSet.Customer;

public class Demo2 {

	public static void main(String[] args) {
		Customer c1 = new Customer(1, "Mohan", "Pune", 9800);
		Customer c2 = new Customer(2, "A", "NY", 98000);
		Customer c3 = new Customer(43, "B", "BLR", 800);
		Customer c4 = new Customer(3, "C", "BOM", 900);
		Customer c5 = new Customer(2, "D", "BLR", 9000);
		
		
		List<Customer> allCustomers = new ArrayList<Customer>();
		allCustomers.add(c1);
		allCustomers.add(c2);
		allCustomers.add(c3);
		allCustomers.add(c4);
		allCustomers.add(c5);
	
		
		display(allCustomers);
		
		System.out.println("\n After sorting by bill amount");
		Collections.sort(allCustomers, new BillAmountComparator());
		display(allCustomers);
		
		System.out.println("\n After sorting by Address");
		Collections.sort(allCustomers, new AddressComparator());
		display(allCustomers);
		
		System.out.println("\n After sorting by ID");
		Collections.sort(allCustomers,new Comparator<Customer>() {

			@Override
			public int compare(Customer o1, Customer o2) {
				if(o1.getCustomerId() < o2.getCustomerId())
					return 1;
				else
					return -1;
			}
			
		});
		display(allCustomers);
		
	}

	static void display(List<Customer> allCustomers) {
		Iterator<Customer> x = allCustomers.iterator();
		
		while(x.hasNext()) {
			System.out.println(x.next());
		}
	}
}
