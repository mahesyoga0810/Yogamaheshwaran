/**
 * 
 */
package q1;

/**
 * @author nexwave
 *
 */
public class Division extends Arithmetic {

	/* (non-Javadoc)
	 * @see q1.Arithmetic#CalculatedValue()
	 */
	public void calculate() {
		try {
			this.num3 = this.num1 / this.num2;
		} catch (ArithmeticException e) {
			System.out.println("Number cannot be divided by 0");
		}
	}

}
