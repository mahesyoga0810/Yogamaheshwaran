package q3;

//import java.util.HashMap;
//import java.util.Map;

public class UserMainCode {

	public static boolean checkTriplets(int[] array) {
		
//		NONCONSEQUTIVE triplets
//		
//		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
//		
//		
//		for (int i : array) {
//			if(map.containsKey(i)) {
//				map.put(i, map.get(i)+1);
//			}else {				
//				map.put(i, 1);
//			}
//		}
		
		int num = Integer.MIN_VALUE,count = 0;
		
		for(int i: array) {
			
			if(num==i) 
				count++;
			else {
				num=i;
				count=1;
			}
			
			if(count==3) 
				return true;
		}
		return false;
	}


}
