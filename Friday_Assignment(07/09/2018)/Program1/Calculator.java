package prgm1;

import java.util.Scanner;

public class Calculator {
	
	public static void main(String[] args) {			
			Arithmetic a[] =new Arithmetic[4];
			
			
			a[0]=new Add();
			a[1]=new Sub();
			a[2]= new Multiple();
			a[3]= new Divide();
			
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter First number:");
			double n1 = sc.nextDouble();
			System.out.println("Enter Second number:");
			double n2 = sc.nextDouble();
			System.out.println("1.Addition\n2.Subtraction\n3.Multiplication\n4.Division");
			System.out.println("Enter your choice:");
			int ch = sc.nextInt();
			sc.close();
			a[0].read(n1, n2);
			a[1].read(n1, n2);
			a[2].read(n1, n2);
			a[3].read(n1, n2);
			
			a[0].calculate();
			a[1].calculate();
			a[2].calculate();
			a[3].calculate();
			
			try {
				a[--ch].display();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				System.out.println("Invalid Choice");
			}
	
			
		
	}
}