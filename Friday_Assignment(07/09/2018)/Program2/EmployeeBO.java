package q2;

public class EmployeeBO {
	

	public void calIncomeTax(EmployeeVo employee) {
		int annualIncome = employee.getAnnualIncome();
		int incomeTax = 0;
		
	
		if(annualIncome > 2000000) 
			incomeTax = (int) ((annualIncome-1000000) * 0.2 + 1250000);
		else if (annualIncome > 1000000) 
			incomeTax = (int) ((annualIncome-500000) * 0.15 + 50000);
		else if (annualIncome > 500000) 
			incomeTax = (int) (annualIncome * 0.1);
		
		employee.setIncomeTax(incomeTax);		
	}
}
